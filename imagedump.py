from errbot import BotPlugin, botcmd

class ImageDump(BotPlugin):
    """Returns list of links to channel images"""

    def activate(self):
        super().activate()

    @botcmd(split_args_with=None)
    def imagelinks(self, msg, args):
        res = self._bot.history('600')
        with open('history.txt', 'w') as f:
            f.write(str(res))
        return "done"
